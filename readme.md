

## CLI Passwords 


## Deployment 

"/Source" directory is the install able files on the hostmachine. Consider it similar to "Dist". 

Dev files are in root directory. 

Update "install.sh" file with new "raw" URLs from BitBucket when "/Source" files change. 

```
python pass.py -s https://google.com -p password -u admin

```

```
python pass.py --site https://google.com --password password --username admin 

```

```
python pass.py --list 

```

```
python pass.py -l  
```

```
python pass.py --remove 1 
 
```

```
python pass.py -r 1

```

```
python pass.py --search google

```

```
pass --site https://google.com --password password --username admin
```

```
pass -v
```

```
pass -l 
```

```
pass --search google

```
