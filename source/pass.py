# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------------

	██████╗ ██╗   ██╗     ██████╗  █████╗ ███████╗███████╗
	██╔══██╗╚██╗ ██╔╝     ██╔══██╗██╔══██╗██╔════╝██╔════╝
	██████╔╝ ╚████╔╝█████╗██████╔╝███████║███████╗███████╗
	██╔═══╝   ╚██╔╝ ╚════╝██╔═══╝ ██╔══██║╚════██║╚════██║
	██║        ██║        ██║     ██║  ██║███████║███████║
	╚═╝        ╚═╝        ╚═╝     ╚═╝  ╚═╝╚══════╝╚══════╝
	
--------------------------------------------------------------------
Usage:
    pass.py
    pass.py -s <site> -p <password> -u <username> 
    pass.py --site <site> --password <password> --username <username>
    pass.py --search <search> 
    pass.py -r <remove> 
    pass.py --remove <remove>
    pass.py -l | --list
    pass.py -h | --help
    pass.py -v | --version
Options:
    <site> Optional site argument. 
    <username> Optional username argument. 
    <password> Optional password argument. 
    <search>   Optional search argument. 
    -l --list  Show all passwords. 
    -h --help  Show this screen.
    -v --version  Show version.
"""

#Type Checking on arguments, order does matter.  
#Duplicates are allowed, don't enter the same values or auto increment continues. 
 
from docopt import docopt
from os.path import expanduser
home = expanduser("~")

import sqlite3

def list_password():
    conn = sqlite3.connect(home  +  '/Passwords/passwords.db')

    cur = conn.cursor()
	
    print('---------------------+----------------------+----------------------+---------------------')
    print_table([('ID','SITENAME','USERNAME','PASSWORD')])
  
    # The result of a "cursor.execute" can be iterated over by row
    for row in cur.execute('SELECT * FROM PASSWORDS;'):
     	#print(row)
   	#print([str(item) for item in row])
   	
    	print_table([([str(item) for item in row])])
	
    # Be sure to close the connection
    conn.close()

def search_password(search_query):
    conn = sqlite3.connect(home  +  '/Passwords/passwords.db')

    cur = conn.cursor()

    CRED = '\033[91m'
    CEND = '\033[0m'

    print('---------------------+----------------------+----------------------+---------------------')
    print(CRED + "Search Found..." + CEND)
    print('---------------------+----------------------+----------------------+---------------------')

    row_string = ''
    query = '%' + search_query + '%'

    print_table([('ID','SITENAME','USERNAME','PASSWORD')])

    # Get list of PASSWORDS 
    for row in cur.execute('SELECT * FROM PASSWORDS WHERE SITE LIKE (?)', [query]):
	row_string += str(row) + "\t"
    	#print(row_string)
	#print([str(item) for item in row])
	print_table([([str(item) for item in row])])

    #do grep on a string inside python output result to variable. 	
    #import os
    #list_of_call = os.popen("cat syscall_list.txt | grep f89e7000 | awk '{print $2}'").read().split('\n')
    #print list_of_call
    #grep -e how -e to -e forge *.txt 	

    # close connection 
    conn.close() 
    	

def create_password(site, password, username):
    conn = sqlite3.connect(home +  '/Passwords/passwords.db')
    print "Opened database successfully";

    conn.execute('''CREATE TABLE IF NOT EXISTS PASSWORDS
        (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
         SITE           TEXT      NOT NULL,
         PASSWORD       CHAR(50)  NOT NULL,
         USERNAME       CHAR(50)  NOT NULL
        );''')
        
    print "Table created successfully";

    conn.execute("INSERT OR IGNORE INTO PASSWORDS (ID,SITE,PASSWORD,USERNAME) \
      VALUES (NULL, ?, ?, ?)", [site, password, username]);

    conn.commit()
    
    print "Records created successfully";

   # print(site)
   # print(password)
   # print(username)
    
    conn.close()

    conn = sqlite3.connect(home + '/Passwords/passwords.db')

    cur = conn.cursor()

    print_table([('ID','SITENAME','USERNAME','PASSWORD')])

    # The result of a "cursor.execute" can be iterated over by row 
    for row in cur.execute('SELECT * FROM PASSWORDS;'):
    	#print(row)
        #print([str(item) for item in row])
	print_table([([str(item) for item in row])])

    # Be sure to close the connection
    conn.close()

    return("Saved... {}!".format(password))

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def remove_password(id):
    try:

        conn = sqlite3.connect(home + '/Passwords/passwords.db')
        cursor = conn.cursor()

        print "Opened database successfully";

	print(isfloat(id))
		
	#convert str to int then test 
	if (isfloat(id) == True) :
	   print("ok")
 	   print(id)
 	   print("Connected to Database")
           # Deleting single record now
           cursor.execute("DELETE FROM PASSWORDS WHERE ID = (?)", [id])
           conn.commit()
           print("Record deleted successfully ")
           cursor.close()

	   conn = sqlite3.connect(home + '/Passwords/passwords.db')

    	   cur = conn.cursor()

  	   print('---------------------+----------------------+----------------------+---------------------')
	   print_table([('ID','SITENAME','USERNAME','PASSWORD')])

    	   # The result of a "cursor.execute" can be iterated over by row
    	   for row in cur.execute('SELECT * FROM PASSWORDS;'):
        	#print(row)
		#print([str(item) for item in row])
		print_table([([str(item) for item in row])])	

    	   # Be sure to close the connection
   	   conn.close()	
           
	else:
	   print("err")
	   
    except sqlite3.Error as error:
        print("Failed to delete record from sqlite table", error)
    finally:
        if (conn):
            conn.close()
            print("the sqlite connection is closed")


def print_header(rows):

    """print_header(rows)
    Prints out a table using the data in `rows`, which is assumed to be a
    sequence of sequences with the 0th element being the header.
    """
    # - figure out column widths
    #widths = [ len(max(columns, key=len)) for columns in zip(*rows) ]

    widths = [len(max(columns, key=len)) for columns in zip(*rows)]
    # - print the header
    header, data = rows[0], rows[1:]
    print(' | '.join( format(title, "%ds" % width) for width, title in zip(widths, header) ))

    # - print the separator
    print( '-+-'.join( '-' * width for width in widths ))

    # - print the data
    for row in data:
        print(" | ".join( format(cdata, "%ds" % width) for width, cdata in zip(widths, row) ))

def print_table(rows):

    """print_table(rows)
    Prints out a table using the data in `rows`, which is assumed to be a
    sequence of sequences with the 0th element being the header.
    """
    # - figure out column widths
    #widths = [ len(max(columns, key=len)) for columns in zip(*rows) ]

    widths = [len(max(columns, key=len)) + abs(len(max(columns, key=len))-20) for columns in zip(*rows)]	
    # - print the header
    header, data = rows[0], rows[1:]
    print(' | '.join( format(title, "%ds" % width) for width, title in zip(widths, header) ))

    # - print the separator
    print( '-+-'.join( '-' * width for width in widths ))

    # - print the data
    for row in data:
        print(" | ".join( format(cdata, "%ds" % width) for width, cdata in zip(widths, row) ))

if __name__ == '__main__':
    arguments = docopt(__doc__, version='DEMO 1.0')
    if arguments['<remove>'] and arguments['-r']:
        #print(arguments)	
	remove_password(arguments['<remove>'])
    elif arguments['<remove>'] and arguments['--remove']: 
        #print(arguments)
        remove_password(arguments['<remove>'])
    elif arguments['--list']: 
    	list_password()
    elif arguments['<site>'] and arguments['-s'] and arguments['<password>'] and arguments['-p'] and arguments['<username>'] and arguments['-u']:
	#print(arguments)
	create_password(arguments['<site>'], arguments['<password>'], arguments['<username>'])
    elif arguments['<site>'] and arguments['--site'] and arguments['<password>'] and arguments['--password'] and arguments['<username>'] and arguments['--username']:
	#print(arguments)
        create_password(arguments['<site>'], arguments['<password>'], arguments['<username>'])
    elif arguments['<search>'] and arguments['--search']: 
        search_password(arguments['<search>'])
    else:
        print(arguments)
