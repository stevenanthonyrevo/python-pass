# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------------

	██████╗ ██╗   ██╗     ██████╗  █████╗ ███████╗███████╗
	██╔══██╗╚██╗ ██╔╝     ██╔══██╗██╔══██╗██╔════╝██╔════╝
	██████╔╝ ╚████╔╝█████╗██████╔╝███████║███████╗███████╗
	██╔═══╝   ╚██╔╝ ╚════╝██╔═══╝ ██╔══██║╚════██║╚════██║
	██║        ██║        ██║     ██║  ██║███████║███████║
	╚═╝        ╚═╝        ╚═╝     ╚═╝  ╚═╝╚══════╝╚══════╝
	
--------------------------------------------------------------------
Usage:
    pass.py
    pass.py -s <site> -p <password> -u <username> 
    pass.py --site <site> --password <password> --username <username>
    pass.py -e <id> <sitename> <sitepassword> <siteusername>
    pass.py --search <search> 
    pass.py -r <remove> 
    pass.py --remove <remove>
    pass.py -l | --list
    pass.py -h | --help
    pass.py -v | --version
Options:
    <site> Optional site argument. 
    <username> Optional username argument. 
    <password> Optional password argument. 
    <search>   Optional search argument. 
    <remove>   Remove by "ID" to delete. 
    -l --list  Show all passwords. 
    -h --help  Show this screen.
    -v --version  Show version.
"""

#Type Checking on arguments, order does matter.  
#Duplicates are allowed, don't enter the same values or auto increment continues. 
 
from docopt import docopt
from os.path import expanduser
home = expanduser("~")

import sqlite3
import os 

def list_password():
  # operating system dependent module 'posix', 'nt', 'java'
  if os.name == 'nt':
    desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Documents')
    Pathlocation = 'r"' + desktop + '/passwords.db' + '"' 
    conn = sqlite3.connect(Pathlocation)
  
  # default macOS/linux
  conn = sqlite3.connect(home + '/Passwords/passwords.db')

  cur = conn.cursor()

  print('---------------------+----------------------+----------------------+---------------------')
  print_table([('ID','SITENAME','PASSWORD','USERNAME')])

  # The result of a "cursor.execute" can be iterated over by row
  for row in cur.execute('SELECT * FROM PASSWORDS;'):
    #print(row)
    #print([str(item) for item in row])
    print_table([([str(item) for item in row])])

  # Be sure to close the connection
  conn.close()

def search_password(search_query):
  #r"C:\Users\vince\Documents\passwords.db"
  # operating system dependent module 'posix', 'nt', 'java'
  if os.name == 'nt':
    desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Documents')
    Pathlocation = 'r"' + desktop + '/passwords.db' + '"' 
    conn = sqlite3.connect(Pathlocation)
  
  # default macOS/linux
  conn = sqlite3.connect(home + '/Passwords/passwords.db')
  cur = conn.cursor()

  CRED = '\033[91m'
  CEND = '\033[0m'

  print('---------------------+----------------------+----------------------+---------------------')
  print(CRED + "Search Found..." + CEND)
  print('---------------------+----------------------+----------------------+---------------------')

  row_string = ''
  query = '%' + search_query + '%'

  print_table([('ID','SITENAME','PASSWORD','USERNAME')])

  # Get list of PASSWORDS 
  for row in cur.execute('SELECT * FROM PASSWORDS WHERE SITE LIKE (?)', [query]):
    row_string += str(row) + "\t"
    #print(row_string)
    #print([str(item) for item in row])
    
  print_table([([str(item) for item in row])])

  #do grep on a string inside python output result to variable. 	
  #import os
  #list_of_call = os.popen("cat syscall_list.txt | grep f89e7000 | awk '{print $2}'").read().split('\n')
  #print list_of_call
  #grep -e how -e to -e forge *.txt 	

  # close connection 
  conn.close() 
    	
def create_password(site, password, username):
  # operating system dependent module 'posix', 'nt', 'java'
  if os.name == 'nt':
    desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Documents')
    Pathlocation = 'r"' + desktop + '/passwords.db' + '"' 
    conn = sqlite3.connect(Pathlocation)
  
  # default macOS/linux
  conn = sqlite3.connect(home + '/Passwords/passwords.db')
  print("Opened database successfully");

  conn.execute('''CREATE TABLE IF NOT EXISTS PASSWORDS
      (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        SITE           TEXT      NOT NULL,
        PASSWORD       CHAR(50)  NOT NULL,
        USERNAME       CHAR(50)  NOT NULL
      );''')
      
  print("Table created successfully");

  conn.execute("INSERT OR IGNORE INTO PASSWORDS (ID,SITE,PASSWORD,USERNAME) \
    VALUES (NULL, ?, ?, ?)", [site, password, username]);

  conn.commit()
  
  print("Records created successfully");

  # print(site)
  # print(password)
  # print(username)
  
  conn.close()

  # operating system dependent module 'posix', 'nt', 'java'
  if os.name == 'nt':
    desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Documents')
    Pathlocation = 'r"' + desktop + '/passwords.db' + '"' 
    conn = sqlite3.connect(Pathlocation)
  
  # default macOS/linux
  conn = sqlite3.connect(home + '/Passwords/passwords.db')

  cur = conn.cursor()

  print_table([('ID','SITENAME','PASSWORD','USERNAME')])

  # The result of a "cursor.execute" can be iterated over by row 
  for row in cur.execute('SELECT * FROM PASSWORDS;'):
    #print(row)
    #print([str(item) for item in row])
	  print_table([([str(item) for item in row])])

  # Be sure to close the connection
  conn.close()

  return("Saved... {}!".format(password))

def edit_row(id, site, password, username):
  # operating system dependent module 'posix', 'nt', 'java'
  if os.name == 'nt':
    desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Documents')
    Pathlocation = 'r"' + desktop + '/passwords.db' + '"' 
    conn = sqlite3.connect(Pathlocation)
  
  # default macOS/linux
  conn = sqlite3.connect(home + '/Passwords/passwords.db')
  print("Opened database successfully");
  
  cur = conn.cursor()

  sqlSite = "UPDATE PASSWORDS SET SITE = %s WHERE ID = %d" 
  sqlPassword = "UPDATE PASSWORDS SET PASSWORD = %s WHERE ID = %d" 
  sqlUsername = "UPDATE PASSWORDS SET USERNAME = %s WHERE ID = %d"

  print(sqlSite, site, id)  
  print(sqlPassword, password, id)
  print(sqlUsername, username, id)
  
  cur.execute("UPDATE PASSWORDS SET SITE = (?) WHERE ID = (?)", (site, id))
  conn.commit()
  
  cur.execute("UPDATE PASSWORDS SET SITE = (?) WHERE ID = (?)", (password, id))
  conn.commit()
  
  cur.execute("UPDATE PASSWORDS SET SITE = (?) WHERE ID = (?)", (username, id))
  conn.commit()

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def remove_password(id):
  try:
    # operating system dependent module 'posix', 'nt', 'java'
    if os.name == 'nt':
      desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Documents')
      Pathlocation = 'r"' + desktop + '/passwords.db' + '"' 
      conn = sqlite3.connect(Pathlocation)
    
    # default macOS/linux
    conn = sqlite3.connect(home + '/Passwords/passwords.db')
    cursor = conn.cursor()
      
    print ("Opened database successfully");
    print(isfloat(id))

    #convert str to int then test 
    if (isfloat(id) == True):
      print("ok")
      print(id)
      print("Connected to Database")
    
      # Deleting single record now
      cursor.execute("DELETE FROM PASSWORDS WHERE ID = (?)", [id])
      conn.commit()
      print("Record deleted successfully ")
      cursor.close()

    # operating system dependent module 'posix', 'nt', 'java'
    if os.name == 'nt':
      desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Documents')
      Pathlocation = 'r"' + desktop + '/passwords.db' + '"' 
      conn = sqlite3.connect(Pathlocation)
    
    # default macOS/linux
    conn = sqlite3.connect(home + '/Passwords/passwords.db')
    cur = conn.cursor()
    
    print('---------------------+----------------------+----------------------+---------------------')
    print_table([('ID','SITENAME','PASSWORD','USERNAME')])
      
    # The result of a "cursor.execute" can be iterated over by row
    for row in cur.execute('SELECT * FROM PASSWORDS;'):
      #print(row)
      #print([str(item) for item in row])
      print_table([([str(item) for item in row])])	

    # Be sure to close the connection
    conn.close()	
     
  finally: 
      if (conn):
        conn.close()
  	    
def print_header(rows):

  """print_header(rows)
  Prints out a table using the data in `rows`, which is assumed to be a
  sequence of sequences with the 0th element being the header.
  """
  # - figure out column widths
  #widths = [ len(max(columns, key=len)) for columns in zip(*rows) ]

  widths = [len(max(columns, key=len)) for columns in zip(*rows)]
  # - print the header
  header, data = rows[0], rows[1:]
  print(' | '.join( format(title, "%ds" % width) for width, title in zip(widths, header) ))

  # - print the separator
  print( '-+-'.join( '-' * width for width in widths ))

  # - print the data
  for row in data:
      print(" | ".join( format(cdata, "%ds" % width) for width, cdata in zip(widths, row) ))

def print_table(rows):

  """print_table(rows)
  Prints out a table using the data in `rows`, which is assumed to be a
  sequence of sequences with the 0th element being the header.
  """
  # - figure out column widths
  #widths = [ len(max(columns, key=len)) for columns in zip(*rows) ]

  widths = [len(max(columns, key=len)) + abs(len(max(columns, key=len))-20) for columns in zip(*rows)]	
  # - print the header
  header, data = rows[0], rows[1:]
  print(' | '.join( format(title, "%ds" % width) for width, title in zip(widths, header) ))

  # - print the separator
  print( '-+-'.join( '-' * width for width in widths ))

  # - print the data
  for row in data:
      print(" | ".join( format(cdata, "%ds" % width) for width, cdata in zip(widths, row) ))

if __name__ == '__main__':
  arguments = docopt(__doc__, version='DEMO 1.0')
  if arguments['<remove>'] and arguments['-r']:
      #print(arguments)	
      remove_password(arguments['<remove>'])
  elif arguments['<remove>'] and arguments['--remove']: 
      #print(arguments)
      remove_password(arguments['<remove>'])
  elif arguments['--list']: 
      list_password()
  elif arguments['<site>'] and arguments['-s'] and arguments['<password>'] and arguments['-p'] and arguments['<username>'] and arguments['-u']:
      #print(arguments)
      create_password(arguments['<site>'], arguments['<password>'], arguments['<username>'])
  elif arguments['<site>'] and arguments['--site'] and arguments['<password>'] and arguments['--password'] and arguments['<username>'] and arguments['--username']:
      #print(arguments)
      create_password(arguments['<site>'], arguments['<password>'], arguments['<username>'])
  elif arguments['-e'] and arguments['<id>'] and arguments['<sitename>'] and arguments['<sitepassword>'] and arguments['<siteusername>']:
      edit_row(arguments['<id>'], arguments['<sitename>'], arguments['<sitepassword>'], arguments['<siteusername>']) 
  elif arguments['<search>'] and arguments['--search']: 
      search_password(arguments['<search>'])
  else:
      print(arguments)