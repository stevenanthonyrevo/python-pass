#!/env/bin/bash 


mkdir -p $HOME/Passwords/
cd $HOME/Passwords/

#download source 
curl https://bitbucket.org/stevenanthonyrevo/python-pass/raw/eb9b4909f6c681d871d44db5f0ac6d6246c052f0/source/docopt.py > docopt.py
curl https://bitbucket.org/stevenanthonyrevo/python-pass/raw/38cf98ce26b14d4fbf0e7c42ca92c2725ec47612/source/pass.py > pass.py 

touch passwords.db

echo "# Password cli tool " >> ~/.bash_profile
echo "alias pass='python /Users/stevenrescigno/Passwords/pass.py'" >> ~/.bash_profile

/bin/bash -c 'python $HOME/Passwords/pass.py --site sample --password sample --username sample'
/bin/bash -c 'python $HOME/Passwords/pass.py  -l'
